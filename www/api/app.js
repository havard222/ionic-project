/**
 * Created by mac on 12/26/14.
 */

angular.module('sobazaar.api', ['sobazaar.models'])

    .constant('urls', {
        'authenticate': '/auth/'
    })

    .provider('api', function () {
        var config = this;

        this.init = function(options) {

            if (!options) {
                throw new Error('You must set options when calling init');
            }

            this.apiServerUrl = options.apiServerUrl
        }


        return {
            init: function(options) {

            },
            $get: function() {
                return {
                    init: function(options) {
                        if (!options) {
                            throw new Error('You must set options when calling init');
                        }

                        this.apiServerUrl = options.apiServerUrl
                    }
                }
            }
        };
    })

    .factory('api', function ($http, $state, $rootScope, $q, CardModel, PaginatorModel, RecommendedUserModel, soCache, ProductModel, ActorModel, UserModel, BrandModel, NotificationModel, ProfileModel) {

        var config = {
            apiServerUrl: 'http://api-noren.sobazaar.com/api'
        }

        var a;

        var api = function(name, options) {
            return a[name](options);
        }

        a =  {
            authenticate: function(provider, accessToken) {
                console.log('api authenticate', provider, accessToken);
                return $http.get(config['apiServerUrl'] + '/auth/' + provider + '/?token=' + accessToken);
            },
            home: function() {
                console.log('api home')
                return $http.get(config['apiServerUrl'] + '/home/').then(function (response) {
                    feed = new PaginatorModel(response.data.feed, CardModel, config['apiServerUrl'] + '/feed/');
                    recommended_users = new PaginatorModel(response.data.recommended_users, RecommendedUserModel);
                    return angular.extend(response.data, {feed: feed, recommended_users: recommended_users});
                });
            },
            user: function(options) {
                console.log('api user...')
                opts = {
                    params: {
                        page_size: 25
                    }
                }
                return $http.get(config['apiServerUrl'] + '/user/' + options.id + '/', opts).then(function (response) {
                    console.log('api user success', response)
                    actor = new ActorModel(response.data.actor)
                    boards = new PaginatorModel(response.data.boards, CardModel, config['apiServerUrl'] + response.data.boards.api_end_point, opts);
                    products = new PaginatorModel(response.data.products, ProductModel, config['apiServerUrl'] + response.data.products.api_end_point, opts);
                    return angular.extend(response.data, {actor: actor, boards: boards, products: products});
                })
            },
            brand: function(options) {
                return $http.get(config['apiServerUrl'] + '/brand/' + options.id + '/').then(function(response) {
                    console.log('api brand', response)

                    for (var i = 0; i < response.data.products.all.items.length; ++i) {
                        c = response.data.products.all.items[i];
                        if (c.actor.uri.indexOf('user') > -1) {
                            c.actor.uri = $state.href('profile.user.board', {id: c.actor.id});
                        } else {
                            console.log('hei')
                            c.actor.uri = $state.href('profile.brand.all', {id: c.actor.id});
                        }
                    }

                    for (var i = 0; i < response.data.products.new.items.length; ++i) {
                        c = response.data.products.new.items[i];
                        if (c.actor.uri.indexOf('user') > -1) {
                            c.actor.uri = $state.href('profile.user.board', {id: c.actor.id});
                        } else {
                            c.actor.uri = $state.href('profile.brand.all', {id: c.actor.id});
                        }
                    }

                    for (var i = 0; i < response.data.products.sale.items.length; ++i) {
                        c = response.data.products.sale.items[i];
                        if (c.actor.uri.indexOf('user') > -1) {
                            c.actor.uri = $state.href('profile.user.board', {id: c.actor.id});
                        } else {
                            c.actor.uri = $state.href('profile.brand.all', {id: c.actor.id});
                        }
                    }

                    return response.data;
                })
            },
            product: function(options) {
                return $http.get(config['apiServerUrl'] + '/product/' + options.id + '/').then(function(response) {
                    console.log('api product', response);
                    more_like_this = []
                    for (var i = 0; i < response.data.more_like_this.length; ++i) {
                        m = new ProductModel(response.data.more_like_this[i])
                        soCache.set(m.id, m)
                        more_like_this.push(m)
                    }
                    product = new ProductModel(response.data.product)
                    soCache.set(product.id, product);
                    angular.extend(response.data, {product: product, more_like_this: {items: more_like_this}});
                    return response.data;
                })
            },
            discover: function() {
                var options = {
                    params: {
                        page_size: 7
                    }
                }
                return $http.get(config['apiServerUrl'] + '/recommended_users/', options).then(function(response) {
                    console.log('api recommended users', response)
                    return new PaginatorModel(response.data, RecommendedUserModel, config['apiServerUrl'] + '/recommended_users/', options);
                })
            },
            recommendedUsers: function() {
                var options = {
                    params: {
                        page_size: 7
                    }
                }
                return $http.get(config['apiServerUrl'] + '/recommended_users/', options).then(function(response) {
                    console.log('api recommended users', response)
                    return new PaginatorModel(response.data, RecommendedUserModel, config['apiServerUrl'] + '/recommended_users/', options);
                })
            },
            search: function(options) {

                var res = {
                    users: $q.defer(),
                    brands: $q.defer(),
                    products: $q.defer()
                };

                console.log(options)
                c = {
                    params: {
                        query: options['query'],
                        page_size: 25
                    },
                    cache: true
                };
                switch (options['type']) {
                    case 'user':
                        return $http.get(config['apiServerUrl'] + '/search/user/', c).then(function(response) {
                            console.log('api search user', response)
                            if (!response.data.api_end_point) {
                                response.data.api_end_point = '/api/search/user/'
                            }
                            paginator = new PaginatorModel(response.data, UserModel, config['apiServerUrl'] + '/search/user/', c);
                            res['users'].resolve(paginator);
                            return paginator;
                        });
                    case 'product':
                        console.log("search for product")
                        return $http.get(config['apiServerUrl'] + '/search/product/', c).then(function(response) {
                                console.log('api search product', response)
                                if (!response.data.api_end_point) {
                                    response.data.api_end_point = '/api/search/product/'
                                }
                                paginator = new PaginatorModel(response.data, ProductModel, config['apiServerUrl'] + '/search/product/', c);
                                res['products'].resolve(paginator);
                                return paginator;
                            });
                    case 'brand':
                        return $http.get(config['apiServerUrl'] + '/search/brand/', c).then(function(response) {
                            console.log('api search brand', response);
                            if (!response.data.api_end_point) {
                                response.data.api_end_point = '/api/search/brand/'
                            }
                            paginator = new PaginatorModel(response.data, BrandModel, config['apiServerUrl'] + '/search/brand/', c);
                            res['brands'].resolve(paginator)
                            return paginator;
                        })
                    };
            },
            love: function(options) {
                return $http.post(config['apiServerUrl'] + '/love/' + options['id'] + '/')
            },
            unlove: function(options) {
                return $http.delete(config['apiServerUrl'] + '/love/' + options['id'] + '/')
            },
            follow: function(options) {
                return $http.post(config['apiServerUrl'] + '/follow/' + options['id'] + '/')
            },
            unfollow: function(options) {
                return $http.delete(config['apiServerUrl'] + '/follow/' + options['id'] + '/')
            },
            followers: function(options) {
                console.log('api followers')
                opts = {
                    params: {
                        reverse: true,
                        page_size: 30
                    }
                }
                return $http.get(config['apiServerUrl'] + '/follow/' + options['id'] + '/', opts).then(function(response) {
                    console.log('api followers success', response)
                    return new PaginatorModel(response.data, UserModel, config['apiServerUrl'] + '/follow/' + options['id'] + '/', opts);
                })
            },
            following: function(options) {
                console.log('api following')
                opts = {
                    params: {
                        page_size: 30
                    }
                }
                return $http.get(config['apiServerUrl'] + '/follow/' + options['id'] + '/', opts).then(function(response) {
                    return new PaginatorModel(response.data, ProfileModel, config['apiServerUrl'] + '/follow/' + options['id'] + '/', opts);
                })
            },
            feed: function(options) {
                console.log('api feed');
                return $http.get(config['apiServerUrl'] + '/feed/', {
                    params: options
                }).then(function(response) {
                    if (!response.data.api_end_point) {
                        response.data.api_end_point = config['apiServerUrl'] + '/feed/'
                    }
                    for (var i = 0; i < response.data.items.length; ++i) {
                        c = response.data.items[i];
                        if (c.header.actor.uri.indexOf('user') > -1) {
                            c.header.actor.uri = $state.href('profile.user.board', {id: c.header.actor.id});
                        } else {
                            c.header.actor.uri = $state.href('profile.brand.all', {id: c.header.actor.id});
                        }
                    }
                    return response.data;
                });
            },
            postComment: function(options) {
                return $http.post(config['apiServerUrl'] + '/comment/' + options['id'] + '/', {
                    data: JSON.stringify(options['comment'])
                }).then(function(response) {
                    return response.data;
                })
            },
            // TODO: comment api should return 'items' instead of 'comments' to be consistent with the rest of the apis
            getComments: function(options) {
                return $http.get(config['apiServerUrl'] + '/comment/' + options['id'] +  '/').then(function(response) {
                    if (!response.data.api_end_point) {
                        response.data.api_end_point = '/api/comment/' + options['id'] + '/'
                    }

                    if (response.data.page < response.data.pages) {
                        response.data.next_page = response.data.page + 1;
                    }
                    response.data.items = response.data.comments;
                    delete response.data.comments;
                    return response.data;
                })
            },
            lovers: function(options) {
                return $http.get(config['apiServerUrl'] + '/love/' + options['id'] + '/', {params: {reverse: true}}).then(function(response) {
                    return response.data;
                })
            },
            board: function(options) {
                console.log('api board')
                return $http.get(config['apiServerUrl'] + '/board/' + options['id'] + '/').then(function(response) {
                    m = new CardModel(response.data)
                    soCache.set(m.id, m);
                    return m;
                })
            },
            notification: function(options) {
                console.log('api news');
                return $http.get(config['apiServerUrl'] + '/notification/').then(function(response) {
                    return new PaginatorModel(response.data, NotificationModel, config['apiServerUrl'] + '/api/notification/');
                })
            }
        }

        for (var key in a) {
            api[key] = a[key];
        }

        return api;
    })

/**
 * Created by mac on 12/28/14.
 */

angular.module('sobazaar.cache', [])

    .provider('soCache', function() {

        var config = {
            cacheDefault: true
        }

        var cache = {}

        function Cache(config) {
            this.returnCached = function(type) {
                return config[type] || config['cacheDefault'];
            }

            this.isCached = function(key) {
                return cache[key] !== undefined;
            }

            this.delete = function(key) {
                delete cache[key];
            }

            this.set = function(key, value) {
                cache[key] = value;
            };

            this.get = function(key) {
                return cache[key];
            }
            /*
            this.get = function(type, api, options) {
                var data = {}

                ret = api(options).then(function(response) {
                    angular.extend(data, response)
                    return response
                })

                if (cache['product']) {
                    console.log('return cached', type)
                    data['product'] = cache['product']
                } else {
                    return ret;
                }

                return data;
            }*/
        }

        this.$get = function() {
            return new Cache(config)
        }
    })

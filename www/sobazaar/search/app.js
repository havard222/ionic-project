/**
 * Created by mac on 12/26/14.
 */

angular.module('sobazaar.search', [])

.config(function($stateProvider) {
        $stateProvider

            .state('search', {
                url: '/search',
                templateUrl: 'sobazaar/search/index.html',
                data: {

                },
                controller: function($scope, $state, api) {
                    console.log('entering search')
                    $scope.config = {}
                    $scope.query = '';
                    $scope.searchResult = {};
                    $scope.search = function(query, type) {
                        query = query || $scope.query;
                        $scope.query = query;
                        if (type) {
                            $scope.data = undefined
                        }
                        /*$state.current.data['data'] = api.search({
                            query: query,
                            type: type || $state.current.name.split('.')[1]
                        });*/
                        api.search({
                            query: query,
                            type: type || $state.current.name.split('.')[1]
                        }).then(function(response) {
                            $scope.data = response;
                        })
                    }
                }
            })

            .state('search.user', {
                url: '/user',
                views: {
                    'content': {
                        template: '<ion-view title="Search user"><ion-content scroll="false"><profilelist style="position: absolute; width: 100%; top: 140px;" class="has-footer" data="data"></profilelist></ion-content></ion-view>',
                        controller: function($scope, $state) {

                        }
                    }
                }
            })

            .state('search.product', {
                url: '/product',
                views: {
                    'content': {
                        template: '<ion-view title="Search product"><ion-content scroll="false"><productlist style="position: absolute; width: 100%; top: 140px;" class="has-footer" data="data"></productlist></ion-content></ion-view>',
                        controller: function($scope, $state) {

                        }
                    }
                }
            })

            .state('search.brand', {
                url: '/brand',
                views: {
                    'content': {
                        template: '<ion-view title="Search brand"><ion-content scroll="false"><profilelist style="position: absolute; widtH: 100%; top: 140px;" class="has-footer" data="data"></profilelist></ion-content></ion-view>',
                        controller: function($scope, $state) {

                        }
                    }
                }
            })
    })
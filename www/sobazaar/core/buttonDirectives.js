/**
 * Created by mac on 12/27/14.
 */

angular.module('sobazaar.core.directives.buttons', [])

    .directive('buyButton', function() {
        return {
            template: '<button class="button button-dark button-block">buy</button>'
        }
    })

    .directive('followButton', function(followingIds, $rootScope, api) {
        return {
            replace: true,
            templateUrl: 'sobazaar/core/templates/followButton.html',
            link: function (scope, elem, attrs) {
                var id = attrs['fbId'],
                    iconButton = attrs['iconButton'] !== undefined,
                    onIcon = 'ion-ios7-checkmark-empty',
                    offIcon = 'ion-plus',
                    onButtonIcon = 'ion-ios7-person',
                    offButtonIcon = 'ion-ios7-person',
                    onText = 'following',
                    offText = 'follow',
                    isFollowing = id in followingIds;

                scope.iconButton = iconButton;
                scope.id = id;

                if (iconButton) {
                    elem.css({'border': 0})
                }

                function toggle() {
                    if (isFollowing) {
                        elem.removeClass(offIcon).addClass(onIcon)
                        if (!iconButton) {
                            scope.text = onText;
                            elem.removeClass('button-dark').addClass('button-light')
                        }
                    } else {
                        elem.removeClass(onIcon).addClass(offIcon)
                        if (!iconButton) {
                            scope.text = offText;
                            elem.removeClass('button-light').addClass('button-dark')
                        }
                    }
                }

                scope.$on('sobazaar.follow.' + id, function() {
                    isFollowing = true;
                    toggle()
                })

                scope.$on('sobazaar.unfollow.' + id, function() {
                    isFollowing = false;
                    toggle()
                })

                toggle();

                elem.on('click', function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    isFollowing = !isFollowing;
                    if (isFollowing) {
                        $rootScope.$broadcast('sobazaar.follow.' + id)
                        api.follow({id : id});
                        followingIds[id] = true;
                        console.log('broadcast: sobazaar.follow.' + id)
                    } else {
                        $rootScope.$broadcast('sobazaar.unfollow.' + id)
                        api.unfollow({id: id});
                        delete followingIds[id];
                        console.log('broadcast: sobazaar.unfollow.' + id)
                    }
                    return false;
                })
            }
        }
    })
/**
 * showCount - shows number of loves beside icon
 * clickable - changes color, broadcast event, sends request to server on click
 * showText - shows love text beside icon
 * hideOnZero - hides the whole directive when count is 0
 * lbId - id of loveable item
 */
    // TODO: Something strange happening here, showing heart but no count, supposed to only show heart with count
    .directive('loveButton', function($rootScope, api, loveIds, soCache, $timeout) {
        return {
            scope: true,
            link: function(scope, elem, attrs) {
                var clickable = attrs['clickable'] !== undefined,
                    showCount = attrs['showCount'] !== undefined,
                    showText  = attrs['showText'] !== undefined,
                    hideOnZero = attrs['hideOnZero'] !== undefined,
                    loved = false,
                    id = attrs['lbId'],
                    data = soCache.get(id)

                scope.showCount = showCount;
                loved = id in loveIds;
                scope.data = data;

                toggle();

                function toggle() {
                    if (clickable) {
                        if (loved) {
                            elem.find('i').css({color: 'red'});
                        } else {
                            elem.find('i').css({color: 'inherit'});
                        }
                    }
                }

                if (showCount) {
                    scope.$watch('data.loveCount', function (n,o) {
                        if (showText) {
                            scope.text = n == 1 ? 'love' : 'loves'
                        }
                        if (!clickable) {
                            elem.toggleClass('ng-hide', !n)
                        }
                    })
                }

                if (clickable) {
                    elem.on('click', function () {
                        loved = !loved;

                        toggle();

                        if (loved) {
                            api.love({id: id})
                            loveIds[id] = true;
                            data.loveCount += 1;
                        } else {
                            api.unlove({id: id})
                            delete loveIds[id];
                            data.loveCount -= 1;
                        }
                    })
                }
            },
            templateUrl: 'sobazaar/core/templates/loveButton.html'
        }
    })

    .directive('commentButton', function(soCache) {
        return {
            scope: true,
            link: function(scope, elem, attrs) {
                var clickable = attrs['clickable'] !== undefined,
                    showCount = attrs['showCount'] !== undefined,
                    showText  = attrs['showText'] !== undefined,
                    hideOnZero = attrs['hideOnZero'] !== undefined,
                    id = attrs['cbId'],
                    data = soCache.get(id);
                scope.data = data;
                if (showCount) {
                    scope.$watch('data.commentCount', function (n,o) {
                        if (showText) {
                            scope.text = n == 1 ? 'comment' : 'comments'
                        }
                        elem.toggleClass('ng-hide', !n)
                    })
                }
            },
            templateUrl: 'sobazaar/core/templates/commentButton.html'
        }
    })

    .directive('shareButton', function() {
        return {
            template: '<i class="icon ion-ios7-upload-outline" share-modal></i>'
        }
    })

/**
 * Created by mac on 12/29/14.
 */

angular.module('sobazaar.models', [])

.factory('CardModel', function(ActorModel, ProductModel, soCache) {

        var CardModel = function(data) {
            this.data = data;
            console.log(data)
            this.id = this.data.header.id;
            this.loveCount = this.data.footer.loves.count;
            this.commentCount = this.data.footer.comments.count;
            this.comments = this.data.footer.comments;
            this.actor = new ActorModel(this.data.header.actor);
            this.title = this.data.header.title;
            this.layout = this.data.body.layout;
            this.tag_type = this.data.body.tag_type;
            this.products = this.data.body.products;

            for (var i = 0; i < this.data.body.products.length; ++i) {
                p = new ProductModel(this.data.body.products[i], true)
                this.data.body.products[i] = p;
                soCache.set(p.getId(), p);
            }
            this.data.footer.comments.items = this.data.footer.comments.comments;
            delete this.data.footer.comments.comments;

            var layout = this.layout;

            switch (layout) {
                case '1_0':
                    layout = '6x6';
                    break;
                case '2_0':
                    layout = '6x3:6x3';
                    break;
                case '2_1':
                    layout = '3x6:3x6';
                    break;
                case '3_0':
                    layout = '3x3:3x6:3x3';
                    break;
                case '3_1':
                    layout = '3x3:3x3:6x3';
                    break;
                case '3_2':
                    layout = '3x6:3x3:3x3';
                    break;
                case '3_3':
                    layout = '6x3:3x3:3x3';
                    break;
                case '4_0':
                    layout = '3x3:3x3:3x3:3x3'
                    break;
                case '6_0':
                    layout = '4x4:2x2:2x2:2x2:2x2:2x2';
                    break;
            }

            this.layout = layout;

            console.log('new card model', this)
        };

        CardModel.prototype.getId = function() {
            return this.data.header.id;
        };

        CardModel.prototype.getActorUri = function() {
            return this.actor.getUri();
        };

        CardModel.prototype.getActorImage = function() {
            return this.actor.getImage();
        };

        CardModel.prototype.getActorName = function() {
            return this.actor.getName();
        };

        CardModel.prototype.getTitle = function() {
            return this.data.header.title;
        };

        CardModel.prototype.getTagType = function() {
            return this.data.body.tag_type;
        };

        CardModel.prototype.getLayout = function() {
            return this.data.body.layout;
        };

        CardModel.prototype.setLayout = function(layout) {
            this.data.body.layout = layout;
        }

        CardModel.prototype.isFashionBoard = function() {
            return this.getTagType() === 'FASHION';
        } ;

        CardModel.prototype.isSaleBoard = function() {
            return this.getTagType() === 'SALE';
        };

        CardModel.prototype.isNewBoard = function() {
            return this.getTagType() === 'NEW';
        };

        CardModel.prototype.isLoveBoard = function() {
            return this.getTagType() === 'LOVE';
        }

        CardModel.prototype.getProducts = function() {
            return this.data.body.products;
        }

        CardModel.prototype.getLoveCount = function() {
            return this.data.footer.loves.count;
        }

        CardModel.prototype.incLoveCount = function() {
            this.data.footer.loves.count++;
        }

        CardModel.prototype.decLoveCount = function() {
            this.data.footer.loves.count--;
        }

        return CardModel;
    })

    .factory('PaginatorModel', function(soCache, $http) {

        function itemsToModel(items, Model) {
            var res = []
            for (var i = 0; i < items.length; ++i) {
                m = new Model(items[i])
                res.push(m)
                soCache.set(m.id, m);
            }

            return res;
        }

        var PaginatorModel = function(data, Model, url, options) {
            this.data = data;
            this.options = options || {}
            if (!this.options['params']) {
                this.options['params'] = {}
            }
            var me = this;
            this.loadMore = function() {
                console.log('load more', this.next_page)
                angular.extend(this.options['params'], {page: this.next_page})
                return $http.get(url, this.options).then(function(response) {
                    console.log('load more success', me.next_page, response)
                    models = itemsToModel(response.data.items, Model);
                    me.items.splice.apply(me.items, [me.items.length, 9e9].concat(models));
                    me.loadingIndicatorPosition = me.data.items.length - positionFromLast;
                    delete response.data.items;
                    angular.extend(me, response.data)
                    console.log(me)
                    return me;
                })
            }

            this.next_page = this.data.next_page;
            this.data.items = itemsToModel(this.data.items, Model)
            this.items = this.data.items;
            var positionFromLast = Math.floor(this.data.items.length/2);
            this.loadingIndicatorPosition = this.data.items.length - positionFromLast;
        };

        PaginatorModel.prototype.getItems = function() {
            return this.data.items;
        };

        return PaginatorModel;
    })

    .factory('UserModel', function(ActorModel) {
        var UserModel = function(data) {
            this.data = data;
            this.id = this.data.id || this.data.actor.id;
            this.image = this.data.image || this.data.profile_image || this.data.actor.profile_image;
            this.name = this.data.name || this.data.actor.name
            this.isUser = true;
            //this.actor = new ActorModel(this.data.actor);
        }
        return UserModel;
    })

    .factory('ActorModel', function() {
        var ActorModel = function(data) {

            //console.log('new actor', data)
            this.data = data;
            this.data.uri = '#' + this.data.uri.replace(/\/$/, '');

            this.id = this.data.id;
            this.uri = this.data.uri;
            this.image = this.data.image || this.data.profile_image;
            this.name = this.data.name;
            this.isBrand = this.uri.indexOf('brand') >= 0;
            this.isUser = this.uri.indexOf('user') >= 0;

            if (this.isUser) {
                this.uri += '/board'
            } else {
                this.uri += '/all'
            }
        }

        ActorModel.prototype.getImage = function() {
            return this.data.image;
        }

        ActorModel.prototype.getName = function() {
            return this.data.name;
        }

        ActorModel.prototype.getUri = function() {
            return this.data.uri;
        }

        ActorModel.prototype.getId = function() {
            return this.data.id;
        }

        return ActorModel;
    })

    .factory('RecommendedUserModel', function(ActorModel) {
        var RecommendedUserModel = function(data) {
            this.data = data;
            this.id = this.data.actor.id
            this.actor = new ActorModel(this.data.actor)
            this.followers = this.data.social.followers
            this.boards = this.data.boards;
            this.isUser = true;
        }

        RecommendedUserModel.prototype.getId = function() {
            return this.data.actor.id;
        }

        return RecommendedUserModel;
    })

    .factory('BoardModel', function() {
        var BoardModel = function(data) {
            this.data = data;
        }
        return BoardModel;
    })

    .factory('ProductModel', function(ActorModel, PriceModel) {
        var ProductModel = function(data, simple) {
            //console.log('new product model', data)
            this.data = data;

            this.id = this.data.id;
            this.image = this.data.image;
            this.images = [this.image]

            if (!simple) {
                this.images = this.data.images;
                this.actor = new ActorModel(this.data.actor);
                this.loveCount = this.data.loves;
                this.name = this.data.name;
                this.price = new PriceModel(this.data.price[0].current)
                this.retail_price = new PriceModel(this.data.price[0].retail)
                this.sale = this.retail_price.value > this.price.value;
            }
        }

        ProductModel.prototype.getId = function() {
            return this.data.id;
        }

        ProductModel.prototype.getImage = function() {
            return this.data.image;
        }

        ProductModel.prototype.update = function() {

        }

        return ProductModel;
    })

    .factory('PriceModel', function() {
        var PriceModel = function(data) {
            //console.log('new price model', data)
            this.data = data;
            this.value = this.data.value;
            this.formatted = this.data.formatted;
        }
        return PriceModel;
    })

    .factory('BrandModel', function() {
        var BrandModel = function(data) {
            this.data = data;
            this.id = this.data.id;
            this.image = this.data.image;
            this.name = this.data.name;
            this.url = '#' + this.data.uri.replace(/\/$/, '')
            this.isBrand = true;
        }
        return BrandModel;
    })

    .factory('NotificationModel', function() {
        var NotificationModel = function(data) {
            this.data = data;
        }
        return NotificationModel;
    })

    .factory('ProfileModel', function(UserModel, BrandModel) {
        var ProfileModel = function(data) {
            console.log(data)
            if (data.type == 'brand') {
                data.uri = '/brand/' + data.id + '/'
                angular.extend(this, new BrandModel(data))
            } else if (data.type == 'user') {
                data.uri = '/user/' + data.id + '/'
                angular.extend(this, new UserModel(data))
            } else if (data.uri.indexOf('brand') >= 0) {
                angular.extend(this, new BrandModel(data))
            } else {
                angular.extend(this, new UserModel(data))
            }
        }
        return ProfileModel;
    })

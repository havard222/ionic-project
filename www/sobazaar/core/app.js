/**
 * Created by mac on 12/26/14.
 */

angular.module('sobazaar.core', ['sobazaar.core.directives'])

.value('loveIds', {})
    .value('followingIds', {})
    .value('followerIds', {})

.factory('socialHttpInterceptor', function(loveIds, followingIds, followerIds) {
        return {
            response: function(response) {
                if (response.config.url.indexOf('api-noren.sobazaar.com/api') >= 0) {
                    if (response.data.viewing_user) {
                        var loves = response.data.viewing_user['loves'],
                            followers = response.data.viewing_user['followers'],
                            following = response.data.viewing_user['following']

                        for (var id in loves) {
                            loveIds[loves[id]] = true;
                        }

                        for (var id in followers) {
                            followerIds[followers[id]] = true;
                        }

                        for (var id in following) {
                            followingIds[following[id]] = true;
                        }
                    }
                }
                return response;
            }
        }
    })

.config(function($httpProvider) {
        $httpProvider.interceptors.push('socialHttpInterceptor');
    })

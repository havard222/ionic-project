/**
 * Created by mac on 12/26/14.
 */

angular.module('sobazaar.core.directives', ['sobazaar.core.directives.buttons'])


    // TODO: find a way to also send correct parameters / data on pagination (e.g. search + query + page_size etc)
    .controller('infiniteScrollListCtrl', function($scope, $timeout, $ionicScrollDelegate) {
        console.log('infinite list')
        $scope.loading = false;
        $scope.loadMore = function() {
            $scope.loading = true;
            console.log('load more', $scope.data)
            $scope.more = false;
            $scope.data.loadMore().finally(function () {
                $timeout(function () {
                    $scope.more = $scope.data.next_page;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    $scope.loading = false;
                    $ionicScrollDelegate.resize()
                }, 1000)
            });
        }

        $timeout(function() {
            if ($scope.data) {
                $scope.more = $scope.data.next_page;
            } else {
                a = $scope.$watch('data', function (n, o) {
                    if (n) {
                        $scope.more = $scope.data.next_page
                        a()
                    }
                })
            }

            $scope.onScroll = ionic.throttle(function (e) {
                console.log('broadcast scroll event', e)
                $scope.$broadcast('scroll.event');
                //console.log('scroll event', angular.element('load-more-trigger'))
            }, 1000)
        }, 2000)
    })

.directive('productPrice', function(soCache) {
        return {
            scope: true,
            link: function(scope, elem, attrs) {
                var id = attrs['ppId'],
                    data = soCache.get(id);

                scope.data = data;
            },
            template: '<span style="text-decoration: line-through;" ng-if="data.sale">{{data.retail_price.value}}</span> {{data.price.formatted}}'
        }
    })

.directive('productInfoButton', function() {
        return {
            template: '<i class="icon ion-information-circled"></i>'
        }
    })

.directive('collapsableContent', function() {
        return {
                scope: {
                    hidden: '=hide'
                },
                transclude: true,
                template: '<div><a class="button button-block icon-right ion-chevron-right button-dark">More from this brand</a><div class="content" ng-hide="hidden"><ng-transclude></ng-transclude></div></div>',
                link: function(scope, elem) {
                    var hidden = true
                    scope.hidden = hidden;
                    // Toggle icon
                    elem.on('click', function() {
                        angular.element(elem).find('a').removeClass(hidden ? 'ion-chevron-right' : 'ion-chevron-down').addClass(hidden ? 'ion-chevron-down' : 'ion-chevron-right');
                        angular.element(elem).find('div.content').toggleClass('ng-hide', hidden)
                        hidden = !hidden;
                        scope.$apply(function () {
                            scope.hidden = hidden;
                        })
                    })
                }
            }
    })

.directive('searchBar', function() {
        return {
            scope: {
                placeholder: '@',
                query: '=',
                search: '=',
                focus: '@'
            },
            replace: true,
            controller: function($scope) {
                if ($scope.placeholder === undefined) {
                    $scope.placeholder = 'search';
                }
                $scope.localQuery = '';

                if ($scope.search === undefined) {
                    $scope.search = function () {
                        $scope.query = $scope.localQuery;
                    }
                }

                $scope.$watch('localQuery', function(n, o) {
                    if (n !== o && n) {
                        // TODO: only update on enter or x seconds after keypress
                        $scope.search(n)
                    }
                })


            },
            templateUrl: 'sobazaar/core/templates/searchBar.html'
        }
    })

    .directive('disabledSearchBar', function() {
        return {
            scope: {
                placeholder: '@'
            },
            controller: function($scope) {
                $scope.placeholder = $scope.placeholder || 'search'
                $scope.disabled = true;
            },
            templateUrl: 'sobazaar/core/templates/searchBar.html'
        }
    })

    .directive('notificationlist', function() {
        return {
            scope: {
                data: '='
            },
            templateUrl: 'sobazaar/core/templates/notificationlist.html',
            controller: 'infiniteScrollListCtrl'
        };
    })
    .directive('productlist', function() {
        return {
            scope: {
                data: '='
            },
            templateUrl: 'sobazaar/core/templates/productlist.html',
            controller: 'infiniteScrollListCtrl'
        }
    })
    .directive('productThumbnailList', function() {
        return {
            scope: {
                data: '=',
                onClick: '&ptlOnClick'
            },
            link: function(scope, elem) {
              scope.emitProduct = function(item) {
                  console.log('emit sobazaar.thumbnail.product', item)
                  scope.$emit('sobazaar.thumbnail.product', item)
              }
            },
            templateUrl: 'sobazaar/core/templates/productThumbnailList.html',
            controller: 'infiniteScrollListCtrl'
        }
    })

    .directive('profilelist', function() {
        return {
            scope: {
                data: '='
            },
            templateUrl: 'sobazaar/core/templates/profilelist.html',
            controller: 'infiniteScrollListCtrl'
        }
    })

    .directive('userlist', function() {
        return {
            scope: {
                'data': '=',
                api: '=',
                apiOptions: '='
            },
            templateUrl: 'sobazaar/core/templates/userlist.html',
            controller: 'infiniteScrollListCtrl'
        }
    })

    .directive('commentlist', function() {
        return {
            scope: {
                'data': '=',
                api: '=',
                apiOptions: '='
            },
            templateUrl: 'sobazaar/core/templates/commentlist.html',
            controller: 'infiniteScrollListCtrl'
        }
    })

    .directive('cardlist', function() {
        return {
            scope: {
                data: '='
            },
            templateUrl: 'sobazaar/core/templates/cardlist.html',
            controller: 'infiniteScrollListCtrl',
            link: function(scope) {
                scope.apiName = 'feed';
            }
        }
    })

    .directive('recommendedUserlist', function() {
        return {
            scope: {
                data: '='
            },
            templateUrl: 'sobazaar/core/templates/recommended_userlist.html',
            controller: 'infiniteScrollListCtrl',
            link: function(scope) {
                scope.apiName = 'recommended_user';
            }
        }
    })

    .directive('carousel', function() {
        return {
            scope: {
              'slides': '='
            },
            templateUrl: 'sobazaar/core/templates/carousel.html'
        }
    })

.directive('followersHorizontal', function() {
        return {
            link: function(scope, elem, attrs) {
                var id = attrs['fhId'],
                    count = Number(attrs['count']);

                scope.count = count;

                scope.$on('sobazaar.follow.' + id, function() {
                    scope.count += 1;
                })

                scope.$on('sobazaar.unfollow.' + id, function() {
                    scope.count -= 1;
                })
            },
            template: '{{count}} followers'
        }
    })

.directive('focusMe', function($timeout) {
        return {
            link: function(scope, elem, attrs) {
                // Sometimes the transition to search page from clicking on the search bar on the discover
                // page only loads half the search page. By increasing the timeout timer from 0 to 500 seems to have solved the problem.
                if (attrs['focusMe']) {
                    $timeout(function() {
                        elem[0].focus()
                    }, 500)
                }
            }
        }
    })

    .directive('singleImageCarousel', function($parse) {
        return {
            link: function(scope, elem, attrs) {
                var images = $parse(attrs)();
                scope.images = images;
            },
            template: '<ion-slide-box show-page="true">' +
                            '<ion-slide ng-repeat="img in data.product.images track by $index">' +
                                '<img width="100%" src="{{img}}">' +
                            '</ion-slide>' +
                        '</ion-slide-box>'
        }
    })

    .directive('renderFashionBoard', function($parse, $compile, $http, $templateCache, soCache, $timeout) {
        return {
            scope: true,
            link: function(scope, elem, attrs) {
                var id = attrs['rfbId'],
                    data = soCache.get(id),
                    layout = attrs['rfbLayout'],
                    activeSquare = attrs['rfbActiveSquare'];

                scope.activeSquare = activeSquare;

                console.log(layout, 'her', attrs)
                if (layout) {
                    data = {
                        layout: layout,
                        products: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                } else {
                    if (data)
                        layout = data.layout;
                }

                var t = soCache.get(id)
                scope.t = t
                scope.$watch('t.layout', function(n,o){
                    if (n && n != o && !(attrs['selectOnClick'] !== undefined)) {
                        console.log('changed layout', attrs['selectOnClick'] === undefined)
                        template = $templateCache.get('/layouts/' + n + '.html');
                        elem.addClass('card-layout');
                        elem.html(template);
                        scope.data = t.products;
                        scope.layout = n;
                        $compile(elem.contents())(scope)
                    }
                });
                $timeout(function() {
                    scope.$watch('layout', function(n,o) {
                        if (n) {
                            console.log('changed layout')
                            template = $templateCache.get('/layouts/' + n + '.html');
                            elem.addClass('card-layout');
                            elem.html(template);
                            scope.data = t.products;
                            scope.layout = n;
                            $compile(elem.contents())(scope)

                        }
                    })
                }, 1000);

                template = $templateCache.get('/layouts/' + layout + '.html');
                elem.addClass('card-layout');
                elem.html('{{layout}}' + template);
                scope.data = data.products;
                scope.layout = layout;
                $compile(elem.contents())(scope)

                if (attrs['selectOnClick'] !== undefined) {
                    elem.on('click', function() {
                        console.log('select on click')

                        soCache.get(id).layout = layout
                        //console.log('emit:', 'sobazaar.board.clicked', layout)
                        //scope.$emit('sobazaar.board.clicked', layout)
                        //scope.layout = layout;
                    })
                }
            }
        }
    })

    .directive('openUp', function() {
        return {
            link: function(scope, elem) {
                console.log('hhhh', elem.clientWidth, elem, elem.offsetWidth, $(elem[0]).outerHeight())
                elem.css('height', elem[0].clientWidth)
                scope.$watch(elem[0].offsetWidth, function(n,o) {
                    console.log('offset', n, o)
                    if (n) {
                        elem.css('height', n)
                    }
                });
                scope.$watch(elem[0].clientWidth, function(n,o) {
                    console.log('client', n, o)
                    if (n) {
                        elem.css('height', n)
                    }
                });
                scope.$watch(elem[0].scrollWidth, function(n,o) {
                    console.log('scroll', n, o)
                    if (n) {
                        elem.css('height', n)
                    }
                });
            }
        }
    })

    .directive('renderEmptyFashionBoard', function($parse, $compile, $http, $templateCache, soCache) {
        return {
            scope: true,
            link: function(scope, elem, attrs) {
                var layout = '6_0';

                // TODO: add rest of old layout ids here

                template = $templateCache.get('/layouts/' + layout + '.html');
                elem.addClass('card-layout');
                elem.html('{{layout}}' + template);
                scope.data = data.products;
                scope.layout = layout;
                $compile(elem.contents())(scope)
            }
        }
    })

    .directive('renderBoard', function(soCache) {
        return {
            scope: true,
            link: function(scope, elem, attrs) {
                var id = attrs['rbId'],
                    data = soCache.get(id)
                scope.data = data.products;
            },
            template:   '<ion-scroll direction="x" class="wide-as-needed">' +
                            '<a ng-repeat="b in data" ui-sref="product({id: b.id})">' +
                                '<img width="40%" src="{{b.image}}">' +
                            '</a>' +
                        '</ion-scroll>'
        }
    })

    .directive('redirectOnClick', function($parse, $state) {
        return {
            link: function(scope, elem, attrs) {
                var redirectOnClick = attrs['redirectOnClick'],
                    redirectOnClickParams = $parse(attrs['redirectOnClickParams'])();

                elem.on('click', function() {
                    $state.go(redirectOnClick, redirectOnClickParams)
                })
            }
        }
    })

    .directive('shareModal', function($ionicModal, $ionicActionSheet) {
        return {
            link: function($scope, elem, attrs) {

                // Triggered on a button click, or some other target
                $scope.show = function() {

                    // Show the action sheet
                    var hideSheet = $ionicActionSheet.show({
                        buttons: [
                            {text: '<b>These buttons doesn\'t work yet!'},
                            {text: '<b>Share</b> This'},
                            {text: 'Move'}
                        ],
                        destructiveText: 'Delete',
                        titleText: 'Modify your album',
                        cancelText: 'Cancel',
                        cancel: function () {
                            // add cancel code..
                        },
                        buttonClicked: function (index) {
                            return true;
                        }
                    });

                };

                /*
                $ionicModal.fromTemplateUrl('/modals/share_modal.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function(modal) {
                    $scope.modal = modal;
                });
                $scope.openModal = function() {
                    $scope.modal.show();
                };
                $scope.closeModal = function() {
                    $scope.modal.hide();
                };
                //Cleanup the modal when we're done with it!
                $scope.$on('$destroy', function() {
                    $scope.modal.remove();
                });
                // Execute action on hide modal
                $scope.$on('modal.hidden', function() {
                    // Execute action
                });
                // Execute action on remove modal
                $scope.$on('modal.removed', function() {
                    // Execute action
                });
                */
                elem.on('click', function() {
                    console.log('open share')
                    $scope.show()
                })
            }
        }
    })

    .directive('editCardModal', function($ionicModal, $ionicActionSheet) {
        return {
            link: function($scope, elem, attrs) {

                // Triggered on a button click, or some other target
                $scope.show = function() {

                    // Show the action sheet
                    var hideSheet = $ionicActionSheet.show({
                        buttons: [
                            {text: '<b>These buttons doesn\'t work yet!'},
                            {text: '<b>Share</b> This'},
                            {text: 'Move'}
                        ],
                        destructiveText: 'Delete',
                        titleText: 'Modify your album',
                        cancelText: 'Cancel',
                        cancel: function () {
                            // add cancel code..
                        },
                        buttonClicked: function (index) {
                            return true;
                        }
                    });

                };
                /*
                $ionicModal.fromTemplateUrl('/modals/edit_card_modal.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function(modal) {
                    $scope.modal = modal;
                });
                $scope.openModal = function() {
                    $scope.modal.show();
                };
                $scope.closeModal = function() {
                    $scope.modal.hide();
                };
                //Cleanup the modal when we're done with it!
                $scope.$on('$destroy', function() {
                    $scope.modal.remove();
                });
                // Execute action on hide modal
                $scope.$on('modal.hidden', function() {
                    // Execute action
                });
                // Execute action on remove modal
                $scope.$on('modal.removed', function() {
                    // Execute action
                });
                */
                elem.on('click', function() {
                    $scope.show()
                })
            }
        }
    })

    .directive('sobazaarCard', function(soCache) {
        return {
            link: function(scope, elem, attrs) {
                var id = attrs['scId'];
                console.log('sobazaarCard', id)
                scope.id = id;
                scope.scData = soCache.get(id);
            },
            templateUrl: 'sobazaar/core/templates/sobazaarCard.html'
        }
    })

    .directive('cacheData', function($parse, soCache) {
        return {
            link: function(scope, elem, attrs) {
                var type = attrs['cacheType'];
                var data = attrs['cacheData'];
                elem.on('click', function() {
                    soCache.set(type, $parse(data)())
                })
            }
        }
    })

    .directive('loadMoreTrigger', function() {
        return {
            scope: {
                onScroll: '&'
            },
            link: function(scope, elem) {

                scope.loading = false;

                // Modified to return true if given element is in or above viewport
                function isElementInViewport (el) {
                    //special bonus for those using jQuery
                    if (typeof jQuery === "function" && el instanceof jQuery) {
                        el = el[0];
                    }

                    var rect = el.getBoundingClientRect();
                    console.log(rect, rect.left >= 0, rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
                    return (
                    //rect.top >= 0 &&
                    rect.left >= 0 &&
                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) // ß&& /*or $(window).height() */
                    //rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
                    );
                }

                scope.$on('scroll.event', function(e) {
                    console.log('scroll fired', isElementInViewport(elem[0]))
                    if (!scope.loading && isElementInViewport(elem[0])) {
                        scope.loading = true;
                        scope.onScroll()
                    }
                });

                scope.$on('scroll.infiniteScrollComplete', function(e) {
                    scope.loading = false;
                    console.log('infinite scroll complete')
                })
            },
        template: '<div>loading: {{loading}}<span ng-if="!loading">load more items when this text is visible</span><span ng-if="loading">Loading...</span></div>'
        }
    })

    .directive('loadingIndicator', function() {
        return {
            template: '<div style="width: 100%; text-align: center;" ng-if="$last && loading">Loading...</div>'
        }
    })

    .directive('endOfFeed', function() {
        return {
            template: '<div style="width: 100%; text-align: center;" ng-if="$last &&!loading && !more">End of feed...</div>'
        }
    })

    .directive('sobazaarInfiniteScroll', function() {
        return {
            templateUrl: 'sobazaar/core/templates/sobazaarInfiniteScroll.html'
        }
    })

    .directive('affixWithinContainer', function($document, $ionicScrollDelegate) {

        var transition = function(element, dy, executeImmediately) {
            element.style[ionic.CSS.TRANSFORM] == 'translate3d(0, -' + dy + 'px, 0)' ||
            executeImmediately ?
                element.style[ionic.CSS.TRANSFORM] = 'translate3d(0, -' + dy + 'px, 0)' :
                ionic.requestAnimationFrame(function() {
                    element.style[ionic.CSS.TRANSFORM] = 'translate3d(0, -' + dy + 'px, 0)';
                });
        };

        return {
            restrict: 'A',
            require: '^$ionicScroll',
            link: function($scope, $element, $attr, $ionicScroll) {
                var $affixContainer = $($element[0]).closest($attr.affixWithinContainer) || $element.parent();
                var customOffset = Number($attr['affixOffset'])
                var top = 0;
                var height = 0;
                var scrollMin = 0;
                var scrollMax = 0;
                var scrollTransition = 0;
                var affixedHeight = 0;
                var updateScrollLimits = ionic.throttle(function(scrollTop) {
                    top = $affixContainer.offset().top;
                    height = $affixContainer.outerHeight(false);
                    affixedHeight = $($element[0]).outerHeight(false);
                    scrollMin = scrollTop + top - customOffset;
                    scrollMax = scrollMin + height;

                    scrollTransition = scrollMax - affixedHeight;


                    //console.log('top:', top, 'height:', height, 'affixedHeight:', affixedHeight, 'scrollMin:', scrollMin, 'scrollMax:', scrollMax, 'scrollTransition:', scrollTransition)
                }, 500, {
                    trailing: false
                });

                var affix = null;
                var unaffix = null;
                var $affixedClone = null;
                var setupAffix = function() {
                    unaffix = null;
                    affix = function() {
                        $affixedClone = $element.clone().addClass('card').css({
                            'z-index': 0,
                            'margin-top': '-1px'
                        })

                        $parent = angular.element('<div style="position: fixed; top: ' + customOffset + 'px; left: 0; right: 0;"class="list sobazaar-content"></div>')
                        $parent.append($affixedClone)

                        $($ionicScroll.element).append($parent);

                        setupUnaffix();
                    };
                };
                var cleanupAffix = function() {
                    $parent && $parent.remove();
                    $parent = null;
                    $affixedClone && $affixedClone.remove();
                    $affixedClone = null;
                };
                var setupUnaffix = function() {
                    affix = null;
                    unaffix = function() {
                        cleanupAffix();
                        setupAffix();
                    };
                };
                $scope.$on('$destroy', cleanupAffix);
                setupAffix();

                var affixedJustNow;
                var scrollTop;
                $($ionicScroll.element).on('scroll', function(event) {
                    scrollTop = (event.detail || event.originalEvent && event.originalEvent.detail).scrollTop - customOffset;
                    updateScrollLimits(scrollTop);
                    //console.log(scrollTop, scrollMin, scrollMax)
                    if (scrollTop >= scrollMin && scrollTop <= scrollMax) {
                        //console.log('hey', scrollTop, scrollTransition)
                        affixedJustNow = affix ? affix() || true : false;
                        if (scrollTop > scrollTransition) {
                            transition($affixedClone[0], Math.floor(scrollTop-scrollTransition), affixedJustNow);
                        } else {
                            transition($affixedClone[0], 0, affixedJustNow);
                        }
                    } else {
                        unaffix && unaffix();
                    }
                });
            }
        }
    })

    .directive('boardLayouts', function() {
        return {
            link: function(scope, elem, attr) {
                var layout = '6_0'
                scope.layouts.push(layout)
            },
            template: '<div><render-empty-fashion-board></render-fashion-board></div>'
        }
    })
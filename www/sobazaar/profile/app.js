/**
 * Created by mac on 12/26/14.
 */

angular.module('sobazaar.profile', [])

    .controller('profileCtrl', function($scope, $state, profile, $rootScope) {
        $scope.profile = profile;

        if ($state.current.data.tabs.board) {
            $scope.activeButton = 'board'
        } else if ($state.current.data.tabs.all) {
            $scope.activeButton = 'all'
        }
        $scope.config = $state.current.data;
        $scope.$on('sobazaar.follow.' + profile.actor.id, function() {
            profile.social.followers.total_count++;
        })
        $scope.$on('sobazaar.unfollow.' + profile.actor.id, function() {
            profile.social.followers.total_count--;
        })

        $scope.showFollowers = function() {
            $state.go('followers', {
                id: profile.actor.id
            });
        }

        $scope.showFollowing = function() {
            $state.go('following', {
                id: profile.actor.id
            })
        }
    })
.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('profile', {
            abstract: true,
            template: '<ion-nav-view></ion-nav-view>'
        })

            .state('profile.user', {
                url: '/user/{id}',
                templateUrl: 'sobazaar/profile/profile.html',
                resolve: {
                  profile: function($stateParams, api) {
                      return api.user({
                          id: $stateParams.id
                      })
                  }
                },
                data: {
                    tabs: {
                        board: true, lovelist: true
                    }
                },
                controller: 'profileCtrl'
            })

            .state('profile.user.board', {
                url: '/board',
                template: '<cardlist style="position: absolute; width: 100%; top: 162px; bottom: 0;" data="data"></cardlist>',
                controller: function($scope, profile) {
                    $scope.data = profile.boards;
                }
            })

            .state('profile.user.lovelist', {
                url: '/lovelist',
                template: '<productlist style="position: absolute; width: 100%; top: 162px; bottom: 0;" data="data"></productlist>',
                controller: function($scope, profile) {
                    $scope.data = profile.products;
                    $scope.$parent.activeButton = 'lovelist';
                }
            })

            .state('profile.brand', {
                url: '/brand/{id}',
                templateUrl: 'sobazaar/profile/profile.html',
                resolve: {
                    profile: function ($stateParams, api) {
                        return api.brand({
                            id: $stateParams.id
                        })
                    }
                },
                data: {
                    hideFollowing: true,
                    tabs: {
                        all: true, new: true, sale: true
                    }
                },
                controller: 'profileCtrl'
            })

            .state('profile.brand.all', {
                url: '/all',
                template: '<productlist data="data"></productlist>', //sobazaar/core/templates/productlist.html',
                controller: function($scope, profile) {
                    $scope.data = profile.products.all
                }
            })

            .state('profile.brand.new', {
                url: '/new',
                template: '<productlist data="data"></productlist>',
                controller: function($scope, profile) {
                    $scope.data = profile.products.new
                }
            })

            .state('profile.brand.sale', {
                url: '/sale',
                template: '<productlist data="data"></productlist>',
                controller: function($scope, profile) {
                    $scope.data = profile.products.sale
                }
            })
    }])

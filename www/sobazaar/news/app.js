/**
 * Created by mac on 12/28/14.
 */

angular.module('sobazaar.news', [])

.config(function($stateProvider) {
        $stateProvider.state('news', {
            url: '/news',
            templateUrl: 'sobazaar/news/news.html',
            resolve: {
                data: function(api) {
                    return api('notification');
                }
            }
        })
    })

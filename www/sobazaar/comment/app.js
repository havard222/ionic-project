/**
 * Created by mac on 12/28/14.
 */

angular.module('sobazaar.comment', [])

.config(function($stateProvider) {
        $stateProvider.state('comment', {
            url: '^/comment/{id}',
            templateUrl: 'sobazaar/comment/comment.html',
            resolve: {
                data: function($stateParams, api) {
                    return api.getComments({id: $stateParams.id})
                }
            },
            controller: function($scope, $rootScope, $stateParams, data, api) {
                $scope.data = data;

                $scope.postComment = function(text) {
                    $scope.data.items.push({
                        actor: {
                            image: $rootScope.me.profile_image,
                            name: $rootScope.me.name,
                        },
                        comment: text
                    });
                    api.postComment({
                        id: $stateParams.id,
                        comment: text
                    });
                }
            }
        });
    })

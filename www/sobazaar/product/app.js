/**
 * Created by mac on 12/26/14.
 */

angular.module('sobazaar.product', [])

    .config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('product', {
            url: '/product/{id}',
            templateUrl: 'sobazaar/product/index.html',
            resolve: {
                data: function($stateParams, api) {
                    return api('product', {id: $stateParams.id});
                }
            },
            controller: function ($scope, data) {
                $scope.data = data;
            }
        });
    }]);


/**
 * Created by mac on 12/28/14.
 */

angular.module('sobazaar.board', [])

.config(function($stateProvider) {
        $stateProvider

            .state('board_select', {
                url: '/board/create/select',
                templateUrl: 'sobazaar/board/board_select.html',
                controller: function(api, soCache, $scope) {
                    var data = {
                        id: 'create_board',
                        layout: '3x3:3x3:3x3:3x3',
                        products: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
                    }

                    soCache.set(data.id, data)

                    var data = soCache.get('create_board')
                    $scope.board = data;
                    api('search', {type: 'product'}).then(function(response) {
                        $scope.data = response;
                    });

                    var items = []
                    for (var i = 0; i < data.layout.split(':').length; ++i) {
                        items.push(data.products[i])
                    }
                    $scope.items = items

                    $scope.$on('sobazaar.thumbnail.product', function(event, item) {
                        for (var i = 0; i < $scope.board.products.length; ++i) {
                            if (!$scope.board.products[i]['image']) {
                                angular.extend($scope.board.products[i], item)
                                break;
                            }
                        }
                        console.log('on', 'sobazaar.thumbnail.product', event, item)
                    })
                }
            })
            .state('board_create', {
                url: '/board/create',
                templateUrl: 'sobazaar/board/board_create.html',
                controller: function($scope, soCache, $state) {
                    var data = {
                        id: 'create_board',
                        layout: '3x3:3x3:3x3:3x3',
                        products: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]
                    }

                    soCache.set(data.id, data)
                    $scope.data = data;

                    $scope.layouts = [
                        '6x6',
                        '3x6:3x6',
                        '6x3:6x3',
                        '6x3:3x3:3x3',
                        '3x3:3x3:6x3',
                        '3x3:3x6:3x3',
                        '3x6:3x3:3x3',
                        '3x3:3x3:3x3:3x3',
                        '4x4:2x4:2x2:2x2:2x2',
                        '4x4:2x2:2x2:4x2:2x2',
                        '4x4:2x2:2x2:2x2:4x2',
                        '4x4:2x2:2x2:2x2:2x2:2x2',
                        '2x4:2x4:2x2:2x2:2x2:4x2',
                        '2x4:2x4:2x2:2x2:2x2:2x2:2x2',
                        '2x2:2x2:2x2:2x2:2x2:2x2:2x2:2x2:2x2',
                        '4x6:2x2:2x2:2x2',
                        '4x4:2x2:2x2:4x2:2x2'
                    ]

                    $scope.$on('sobazaar.board.clicked', function(event, layout) {
                        console.log('sobazaar.board.clicked', layout, 'change layout')
                        data.layout = layout;
                        event.preventDefault();
                        event.stopPropagation();
                        return false;
                    })

                    $scope.selectProduct = function($event) {
                        console.log('open select product view')
                        $event.stopPropagation();
                        $event.preventDefault();
                        $event.prev
                        $state.go('board_select')
                        return false;
                    }
                }
            })

            .state('board', {
                url: '^/board/{id}',
                templateUrl: 'sobazaar/board/board.html',
                resolve: {
                    data: function($stateParams, api) {
                        return api('board', {id: $stateParams.id});
                    }
                },
                controller: function($scope, data) {
                    $scope.data = data;
                }
            })
    })

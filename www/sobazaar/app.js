// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

/**
 * TODO:
 *
 *  up next:
 *  - make 2 different templates for followbutton with 2 lines of text and followbutton with 1 line of text
 *  - try to make the follow button class=icon to position it to the right
 *
 *  pages:
 *  - create board
 *  - board detail
 *
 *  major:
 *  - carousel
 *  - search filter
 *  - search sort
 *  - pagination
 *
 *  minor:
 *  - Timestamp in card header / followbutton in header
 *  - prefilled search page (brands, products, users...)
 */
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'sobazaar.auth', 'sobazaar.api', 'sobazaar.home', 'sobazaar.profile', 'sobazaar.product', 'sobazaar.core', 'sobazaar.discover', 'sobazaar.search', 'sobazaar.board', 'sobazaar.comment', 'sobazaar.cache', 'sobazaar.news', 'sobazaar.models'])


.run(function($ionicPlatform, auth, $rootScope, $log) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });

    $rootScope.$on('sobazaar.follow', function(event) {
        console.log('follow', event)
    })

    auth.hookEvents();
        auth.getProfile()

        $rootScope.fbLogin = function() {
            console.log('fbLogin clicked')
            auth.signin({}, function (profile, accessToken, state) {
                console.log('login success', profile, accessToken, state)
                /*if ($stateParams.redirect) {
                    $state.go($stateParams.redirect)
                }*/
            }, function(err) {
                console.log('login error', err)
            });
        }
        $rootScope.fbLogout = function() {
            console.log('fbLogout clicked')
            openFB.logout(function(response) {
                console.log('logged out?', response)
            })
            auth.signout(function(response) {
              console.log('success logout', response)
                $http.get('http://api-noren.sobazaar.com/api/logout/').then(function(response) {
                    console.log('lougot of backend', response)
                })
            }, function(response) {
                console.log('error logout', response)
            });
        }
})

.config(function($stateProvider, $urlRouterProvider, authProvider, $httpProvider) {

        $httpProvider.defaults.withCredentials = true;
      $stateProvider

          .state('followers', {
              url: '/followers/{id}',
              templateUrl: 'sobazaar/followers.html',
              controller: function($stateParams, api, $scope) {
                  api.followers({id: $stateParams.id}).then(function (response) {
                      $scope.data = response;
                  })
              }
          })

          .state('following', {
              url: '/following/{id}',
              templateUrl: 'sobazaar/following.html',
              controller: function($stateParams, api, $scope) {

                  api.following({id: $stateParams.id}).then(function (response) {
                      $scope.data = response;
                  })
              }
          })

          .state('lovers', {
              url: '/lovers/{id}',
              templateUrl: 'sobazaar/lovers.html',
              controller: function($stateParams, api, $scope) {
                  api.lovers({id: $stateParams.id}).then(function (response) {
                      $scope.data = response;
                  })
              }
          })

          .state('info', {
          url: '/info',
          templateUrl: 'tmp/info.html',
          controller: function () {

          }
          })
          .state('login', {
          url: '/landing_page/{redirect}',
          templateUrl: 'landing_page.html',
          controller: function($scope, auth, $rootScope, $state, $stateParams, auth) {
              $scope.profile = auth.profile;
              $scope.fbLogin = function() {
                  auth.signin({}, function (profile, accessToken, state) {
                      console.log('success', profile, accessToken, state)
                      if ($stateParams.redirect) {
                          $state.go($stateParams.redirect)
                      }
                  }, function(err) {
                      console.log('error', err)
                  });
              }
              $scope.fbLogout = function() {
                  auth.signout();
              }
          },
              data: {
                  callbackUrl: null
              }
          });

        authProvider.on('loginSuccess', function($location) {
            console.log('loginSuccess: login success')
        });

        authProvider.on('authenticated', function($location) {
            console.log('authenticated: already logged in')
            // This is after a refresh of the page
            // If the user is still authenticated, you get this event
        });

        authProvider.on('loginFailure', function($location, error) {
            console.log('loginFailure: login failed')
        });
      authProvider.init({
        clientID: '...',
        loginState: 'login',
        callbackUrl: location.href,
          authUrl: 'http://api-noren.sobazaar.com/api/auth/facebook/'
      });

        // TODO: Figure out why this isn't working
        /*
        apiProvider.init({
            apiServerUrl: 'api-noren.sobazaar.com/api'
        });
        */


  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

      .state('api', {
          url: '/api',
          templateUrl: 'api/index.html',
          controller: function($scope, api, auth) {
              console.log(auth)
              $scope.authenticate = function() {
                  api.authenticate('facebook', auth.accessToken).then(function(response) {
                      $scope.success = response;
                  }, function(response) {
                      $scope.error = response;
                  })
              }
          }
      })
  // setup an abstract state for the tabs directive
    .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    },
        data: {
          requiresLogin: true
        }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.friends', {
      url: '/friends',
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-friends.html',
          controller: 'FriendsCtrl'
        }
      }
    })
    .state('tab.friend-detail', {
      url: '/friend/:friendId',
      views: {
        'tab-friends': {
          templateUrl: 'templates/friend-detail.html',
          controller: 'FriendDetailCtrl'
        }
      }
    })

  .state('tab.settings', {
    url: '/settings',
    views: {
      'tab-settings': {
        templateUrl: 'templates/tab-settings.html',
        controller: function($scope, auth) {
            $scope.settings = {
                enableFriends: true,
                isAuthenticated: auth.isAuthenticated
            };

            $scope.auth = auth;

            $scope.$watch('settings.isAuthenticated', function (n,o) {
                if (n !== o) {
                    if (n) {
                        auth.signin()
                    } else {
                        auth.signout()
                    }
                }
            });
        }
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});

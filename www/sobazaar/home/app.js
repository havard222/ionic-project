/**
 * Created by mac on 12/26/14.
 */

angular.module('sobazaar.home', ['sobazaar.api'])

.config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('home', {
            url: '/home',
            templateUrl: 'sobazaar/home/home.html',
            data: {
                requiresLogin: true
            },
            controller: ['$scope', 'api', function($scope, api) {
                api('home').then(function(response) {
                    $scope.data = response
                });
            }]
        })
    }])

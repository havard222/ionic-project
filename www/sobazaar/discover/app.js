/**
 * Created by mac on 12/26/14.
 */

angular.module('sobazaar.discover', [])

.config(function($stateProvider) {
        $stateProvider.state('discover', {
            url: '/discover',
            templateUrl: 'sobazaar/discover/index.html',
            resolve: {
                data: function(api) {
                    return api.discover()
                }
            },
            controller: function($scope, data) {
                console.log('entering discover')
                $scope.data = data;
            }
        })
    })

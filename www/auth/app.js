/**
 * Created by mac on 12/20/14.
 */
// Based on angular-auth0
(function () {
    angular.module('sobazaar.auth', ['sobazaar.auth.service', 'sobazaar.auth.utils'])
        .run(function(auth) {
            auth.hookEvents();
        })
    angular.module('sobazaar.auth.utils', [])
        .provider('authUtils', function () {
            var Utils = {
                capitalize: function (string) {
                    return string ? string.charAt(0).toUpperCase() + string.substring(1).toLowerCase() : null;
                }
            };

            angular.extend(this, Utils);

            this.$get = function($rootScope, $q) {
                var authUtils = {}
                angular.extend(authUtils, utils);

                authUtils.promisify = function (nodeback, self) {
                    if (angular.isFunction(nodeback)) {
                        return function (args) {
                            args = Array.prototype.slice.call(arguments);
                            var dfd = $q.defer();
                            var callback = function (err, response, etc) {
                                if (err) {
                                    dfd.reject(err);
                                    return;
                                }
                                // if more arguments then turn into an array for .spread()
                                etc = Array.prototype.slice.call(arguments, 1);
                                dfd.resolve(etc.length > 1 ? etc : response);
                            };

                            args.push(authUtils.applied(callback));
                            nodeback.apply(self, args);
                            // spread polyfill only for promisify
                            dfd.promise.spread = dfd.promise.spread || function (fulfilled, rejected) {
                                return dfd.promise.then(function (array) {
                                    return Array.isArray(array) ? fulfilled.apply(null, array) : fulfilled(array);
                                }, rejected);
                            };
                            return dfd.promise;
                        };
                    }
                };

                return authUtils;
            };
        })

    angular.module('sobazaar.auth.service', ['sobazaar.auth.utils'])
        .provider('auth', function (authUtilsProvider) {

            var defaultOptions = {
                callbackOnLocationHash: true
            };

            var config = this;

            this.init = function (options) {

                if (!options) {
                    throw new Error('You must set options when calling init')
                }

                this.loginUrl = options.loginUrl;
                this.loginState = options.loginState;
                this.clientID = options.clientID || options.clientId;
                this.authUrl = options.authUrl;
                this.initialized = true;
            };

            this.eventHandlers = {}

            this.on = function (anEvent, handler) {
                if (!this.eventHandlers[anEvent]) {
                    this.eventHandlers[anEvent] = []
                }
                this.eventHandlers[anEvent].push(handler)
            };

            var events = ['loginSuccess', 'loginFailure', 'logout', 'forbidden', 'authenticated'];
            angular.forEach(events, function (anEvent) {
                config['add' + authUtilsProvider.capitalize(anEvent) + 'Handler'] = function (handler) {
                    config.on(anEvent, handler);
                }
            });

            this.$get = function ($rootScope, $injector, $http) {
                var auth = {
                    isAuthenticated: false
                };

                var getHandlers = function (anEvent) {
                    return config.eventHandlers[anEvent];
                };

                var callHandler = function (anEvent, locals) {
                    $rootScope.$broadcast('sobazaar.auth.' + anEvent, locals);
                    angular.forEach(getHandlers(anEvent) || [], function (handler) {
                        $injector.invoke(handler, auth, locals)
                    })
                };

                var onSigninOk = function (accessToken, state, profile, isRefresh) {//idToken, accessToken, state, refreshToken, profile, isRefresh) {
                    var profilePromise = auth.getProfile(accessToken);

                    var response = {
                        accessToken: accessToken,
                        state: state,
                        profile: profile,
                        isAuthenticated: true
                    };

                    angular.extend(auth, response);
                    callHandler(!isRefresh ? 'loginSuccess' : 'authenticated', angular.extend({
                        profilePromise: profilePromise
                    }, response));

                    return profilePromise;
                };

                function forbidden() {
                    if (config.loginUrl) {
                        $locaion.path(config.loginUrl)
                    } else if (config.loginState) {
                        $injector.get('$state').go(config.loginState);
                    } else {
                        callHandler('forbidden');
                    }
                }


                $rootScope.$on('$locationChangeStart', function () {
                    if (!config.initialized) {
                        return;
                    }

                    if (!auth.isAuthenticated) {
                        openFB.getLoginStatus(function (response) {

                            // Already connected
                            console.log(response)
                            if (response.status === 'connected') {
                                onSigninOk(response.authResponse.token, response.status, null, true);
                                return;
                            } else {
                                console.log('initialized')
                                auth.profile = {
                                    state: 'initialized'
                                }
                            }

                        })
                    }
                })

                $rootScope.$on('sobazaar.auth.forbiddenRequest', function () {
                    forbidden();
                });

                if (config.loginUrl) {
                    $rootScope.$on('$routeChangeStart', function (e, nextRoute) {
                        if (!config.initialized) {
                            return;
                        }
                        if (nextRoute.$$route && nextRoute.$$route.requiresLogin) {
                            if (!auth.isAuthenticated && !auth.refreshTokenPromise) {
                                console.log('need to log in', $location.path())
                                $rootScope.callbackUrl = $location.path()
                                $location.path(config.loginUrl);
                            }
                        }
                    });
                }

                if (config.loginState) {
                    $rootScope.$on('$stateChangeStart', function (e, to) {
                        if (!config.initialized) {
                            return;
                        }
                        console.log('hei', to)
                        if (to.data && to.data.requiresLogin) {

                            console.log('requires log in')
                            if (!auth.isAuthenticated && !auth.refreshTokenPromise) {
                                console.log('not logged in')
                                e.preventDefault();
                                $injector.get('$state').go(config.loginState, {'redirect': to.name});
                            }
                            console.log('already logged in')
                        }
                    });
                }

                ////////////////////////
                // Start auth service
                //////////////////////////////////////////////

                auth.config = config

                var checkHandlers = function (options, successCallback) {
                    var successHandlers = getHandlers('loginSuccess');
                    if (!successHandlers && !options.username && !options.email && (!successHandlers || successHandlers.length === 0)) {
                        throw new Error('You must define a loginSuccess handler ' +
                        'if not using popup mode or not doing ro call because that means you are doing a redirect');
                    }
                };

                auth.hookEvents = function () {
                    // Does nothing. Hook events on application's run
                }

                // TODO: What does angular.bind do?
                auth.init = angular.bind(config, config.init);

                // TODO: Do we need the 3 functions below?
                auth.getToken = function (options) {
                    throw new Error('Not implemented');
                }

                auth.refreshIdToken = function (refresh_token) {
                    throw new Error('Not implemented');
                }

                auth.renewIdToken = function (id_token) {
                    throw new Error('Not implemented');
                }

                auth.signin = function (options, successCallback, errorCallback, libName) {
                    options = options || {};
                    checkHandlers(options, successCallback, errorCallback);

                    // TODO: What does the below line do?
                    // options = getInnerLibraryConfigField('parseOptions', libName)(options);

                    // TODO: What does the below line do?
                    // var signinMethod = getInnerLibraryMethod('signin', libName);

                    var successFn = /*!successCallback ? null :*/ function (accessToken, state) {
                        onSigninOk(accessToken, state).then(function (profile) {
                            if (successCallback) {
                                successCallback(profile, accessToken, state);
                            }
                        });
                    };

                    var errorFn = /*!errorCallback ? null :*/ function (err) {
                        callHandler('loginFailure', {error: err});
                        if (errorCallback) {
                            errorCallback(err);
                        }
                    };

                    console.log(errorFn)

                    // TODO: What does the 2 lines below do?
                    // var signinCall = authUtils.callbackify(signinMethod, successFn, errorFn , innerAuth0libraryConfiguration[libName || config.lib].library());
                    // signinCall(options);
                    openFB.login(
                        function(response) {
                            if (response.status === 'connected') {
                                successFn(response.authResponse.token, response.state);
                            } else {
                                if (errorFn) {
                                    errorFn(response);
                                }
                            }
                        },
                        {scope:'email,publish_actions'}
                    );
                };

                auth.signup = function (options, successCallback, errorCallback) {
                    options = options || {};
                    checkHandlers(options, successCallback, errorCallback);

                    // options = getInnerLibraryConfigField('parseOptions')(options);

                    var successFn = !successCallback ? null : function (profile, idToken, accessToken, state, refreshToken) {
                        if (!angular.isUndefined(options.auto_login) && !options.auto_login) {
                            successCallback();
                        } else {
                            onSigninOk(idToken, accessToken, state, refreshToken, profile).then(function (profile) {
                                if (successCallback) {
                                    successCallback(profile, idToken, accessToken, state, refreshToken);
                                }
                            });
                        }
                    };

                    var errorFn = !errorCallback ? null : function (err) {
                        callHandler('loginFailure', {error: err});
                        if (errorCallback) {
                            errorCallback(err);
                        }
                    };

                    // var auth0lib = config.auth0lib;
                    // var signupCall = authUtils.callbackify(getInnerLibraryMethod('signup'),successFn , errorFn, auth0lib);

                    // signupCall(options);
                };

                auth.reset = function (options, successCallback, errorCallback) {
                    options = options || {};

                    // options = getInnerLibraryConfigField('parseOptions')(options);
                    // var auth0lib = config.auth0lib;
                    // var resetCall = authUtils.callbackify(getInnerLibraryMethod('reset'), successCallback, errorCallback, auth0lib);

                    // resetCall(options);
                };

                auth.signout = function () {
                    auth.isAuthenticated = false;
                    auth.profile = null;
                    auth.profilePromise = null;
                    auth.idToken = null;
                    auth.state = null;
                    auth.accessToken = null;
                    auth.tokenPayload = null;
                    openFB.logout();
                    console.log('auth.signout')
                    callHandler('logout');
                };

                auth.authenticate = function (profile, idToken, accessToken, state, refreshToken) {
                    return onSigninOk(idToken, accessToken, state, refreshToken, profile, true);
                };

                auth.getProfile = function (accessToken) {
                    return $http.get(config.authUrl + '?token=' + accessToken).then(function(response) {
                        auth.profile = auth.profile || {

                        }
                        angular.extend(auth.profile, response.data)
                        // TODO: quick haq to get profile, should be stored in a variable in auth and use that instead
                        $rootScope.me = response.data;
                        return auth.profile;
                    })
                    //return auth.profile;
                };

                return auth;
            };
        });
}());